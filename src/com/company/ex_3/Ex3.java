package com.company.ex_3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Ex3 {
    public static void init(){
        System.out.println("****** Exercice 2 **********");

        FactorialGenerator factorialGenerator = new FactorialGenerator();
        Map<Integer, Integer> factorials = new HashMap<Integer, Integer>();

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = sc.nextInt();

        List<Integer> digits = factorialGenerator.separateInDigits(number);

        for(int digit: digits){
            if ((digit%2) == 0){
                int factorial = factorialGenerator.generate(digit);
                factorials.put(digit, factorial);
            }
        }

        for(int digit: factorials.keySet()){
            System.out.println("digit " + digit + ", factorial: " + factorials.get(digit));
        }

    }
}
