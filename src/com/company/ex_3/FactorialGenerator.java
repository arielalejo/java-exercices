package com.company.ex_3;

import com.company.ex_2.MinorPrimeDigitDetector;


public class FactorialGenerator extends MinorPrimeDigitDetector {

    int generate(int number) {
        int i,factorial = 1;

        for(i=1;i<=number;i++){
            factorial=factorial*i;
        }
        return factorial;
    }
}
