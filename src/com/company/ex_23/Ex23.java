package com.company.ex_23;

import java.util.Scanner;

public class Ex23 {
    public static void init(){
        System.out.println("****** Exercice 3, Part2 **********");

        GeometricCalculator geometricCalculator = new GeometricCalculator();

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter circle radius: ");
        int radius = sc.nextInt();
        System.out.print("Enter square side: ");
        int side = sc.nextInt();
        System.out.print("Enter base: ");
        int base = sc.nextInt();
        System.out.print("Enter height: ");
        int height = sc.nextInt();

        Circle circle = new Circle(radius);
        Square square = new Square(side);
        Triangle triangle = new Triangle(base, height);
        Rectangle rectangle = new Rectangle(base, height);

        System.out.println("Area of Circle: " + geometricCalculator.calculateCircleArea(circle));
        System.out.println("Area of Triangle: " + geometricCalculator.calculateTriangleArea(triangle));
        System.out.println("Area of Rectangle: " + geometricCalculator.calculateRectangleArea(rectangle));
        System.out.println("Area of Square: " + geometricCalculator.calculateSquareArea(square));

    }
}
