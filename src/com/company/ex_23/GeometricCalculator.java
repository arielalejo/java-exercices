package com.company.ex_23;

public class GeometricCalculator {

    double calculateCircleArea(Circle c){
        double radius = c.getRadius();
        return Math.PI*radius*radius;
    }

    int calculateSquareArea(Square s){
        int side = s.getSide();
        return side*side;
    }

    int calculateTriangleArea(Triangle t){
        int base = t.getBase();
        int height = t.getHeight();
        return base*height/2;
    }

    int calculateRectangleArea(Rectangle r){
        int base = r.getBase();
        int height = r.getHeight();
        return base*height;
    }
}
