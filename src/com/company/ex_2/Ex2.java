package com.company.ex_2;

import com.company.ex_1.PrimeDetector;

import java.util.*;

public class Ex2 {
    public static void init(){
        System.out.println("****** Exercice 2 **********");

        PrimeDetector primeDetector = new PrimeDetector();
        MinorPrimeDigitDetector minorPrimeDigitDetector = new MinorPrimeDigitDetector();
        List<Integer> primeNumbers = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = sc.nextInt();

        List<Integer> digits = minorPrimeDigitDetector.separateInDigits(number);
        for(int num: digits){
            if (primeDetector.detectPrime(num)){
                primeNumbers.add(num);
            }
        }

        List<Integer> sortedNumbers = minorPrimeDigitDetector.sortArray(primeNumbers);
        int size = sortedNumbers.size();
        if ( size == 0) { System.out.println(-1);}
        else {System.out.println("Minor Prime: " + sortedNumbers.get(0));}
    }

}
