package com.company.ex_2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinorPrimeDigitDetector extends AbstractMinorPrime {

    @Override
    public List<Integer> separateInDigits(int number) {
        List<Integer> digits = new ArrayList<Integer>();
        while(number > 0) {
            digits.add(number % 10);
            number /= 10;
        }
        return digits;
    }

    @Override
    public List<Integer> sortArray(List<Integer> array) {
        Collections.sort(array);
        return array;
    }
}
