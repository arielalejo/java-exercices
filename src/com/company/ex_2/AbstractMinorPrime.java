package com.company.ex_2;

import java.util.List;

public abstract class AbstractMinorPrime {
    abstract List<Integer> separateInDigits(int number);

    abstract List<Integer> sortArray(List<Integer> array);

}
