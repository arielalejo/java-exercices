package com.company.ex_22;

import java.util.Scanner;

public class Ex22 {
    public static void init(){
        System.out.println("****** Exercice 2, Part2 **********");

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int note1 = sc.nextInt();
        System.out.print("Enter a number: ");
        int note2 = sc.nextInt();
        System.out.print("Enter a number: ");
        int note3 = sc.nextInt();

        Calculator notesCalculator = new Calculator(note1, note2, note3);

        System.out.println("Sum of notes: " );
        System.out.println(notesCalculator.calculateSum());

        System.out.println("Mean of notes: " );
        System.out.println(notesCalculator.calculateMean());

        System.out.println("double of notes: " );
        System.out.println( notesCalculator.doubleNotes());
    }
}
