package com.company.ex_22;

import java.util.List;

public abstract class GenericCalculator {
    protected int note1;
    protected int note2;
    protected int note3;

    public GenericCalculator(int note1, int note2, int note3){
        this.note1 = note1;
        this.note2 = note2;
        this.note3 = note3;
    }

    abstract int calculateSum();

    abstract int calculateMean();

    abstract List<Integer> doubleNotes();
}
