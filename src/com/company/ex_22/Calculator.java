package com.company.ex_22;

import java.util.ArrayList;
import java.util.List;

public class Calculator extends GenericCalculator {

    public Calculator(int note1, int note2, int note3) {
        super(note1, note2, note3);
    }

    @Override
    int calculateSum() {
        return (this.note1+ this.note2 + this.note3 ) ;
    }

    @Override
    int calculateMean() {
        return calculateSum()/3;
    }

    @Override
    List<Integer> doubleNotes() {
        List<Integer> newNotes = new ArrayList<Integer>();
        newNotes.add(this.note1 * 2);
        newNotes.add(this.note2 * 2);
        newNotes.add(this.note3 * 2);

        return newNotes;
    }

}
