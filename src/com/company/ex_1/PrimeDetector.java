package com.company.ex_1;

public class PrimeDetector extends AbstractPrimeDetector{
    @Override
    public boolean detectPrime(int num) {
        boolean isPrime=true;

        for(int i=2;i<=num/2;i++)
        {
            if((num%i)==0)
            {
                isPrime=false;
                break;
            }
        }

        return isPrime;
    }

    @Override
    public boolean detectPair(int number) {
        return number % 2 == 0;
    }
}
