package com.company.ex_1;

public abstract class AbstractPrimeDetector {

     protected abstract boolean detectPrime(int num);

     abstract boolean detectPair(int number);
}
