package com.company.ex_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex1 {
    public static void init() {
        {
            System.out.println("****** Exercice 1 **********");

            PrimeDetector detector = new PrimeDetector();
            List<Integer> primeNumbers = new ArrayList<Integer>();
            List<Integer> restNumbers = new ArrayList<Integer>();

            Scanner scanner = new Scanner(System.in);
            int input = 1;
            while (!detector.detectPair(input)){
                System.out.print("Enter pair number to quite: ");
                input = scanner.nextInt();

                if(detector.detectPrime(input)) {primeNumbers.add(input);}
                else {restNumbers.add(input);}
            }

            System.out.println("Prime numbers:");
            for(int num: primeNumbers){System.out.println(num);}
            System.out.println("Rest of numbers:");
            for(int num: restNumbers){System.out.println(num);}


        }
    }
}
