package com.company.ex_24;

import java.util.Scanner;

public class Ex24 {

    public static void init(){
        System.out.println("****** Exercice 4, Part2 **********");

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = sc.nextInt();

        DigitCalculator digitCalculator = new DigitCalculator(number);

        System.out.println("total of primes: " + digitCalculator.totalPrimeDigits());
        System.out.println("total of perfects: " + digitCalculator.totalPerfectDigits());
        System.out.println("sum of digits: " + digitCalculator.sumDigits());



    }
}
