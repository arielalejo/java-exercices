package com.company.ex_24;

import java.util.ArrayList;
import java.util.List;

public class DigitCalculator {
    private int number;
    List<Integer> digits;

    public DigitCalculator(int number) {
        this.number = number;
        this.digits = this.separateInDigits();
    }

    int totalPrimeDigits(){
        int totalOfPrimes = 0;

        for(int digit: digits){
            if(detectPrime(digit)){totalOfPrimes += 1;}
        }

        return totalOfPrimes;

    }

    int totalPerfectDigits(){
        int totalOfPerfects = 0;
        for (int digit: digits){
            if (detectPerfect(digit)){ totalOfPerfects += 1;}
        }
        return totalOfPerfects;
    }

    int sumDigits(){
        int sum = 0;
        for (int digit: digits){
            sum += digit;
        }

        return sum;
    }

    private List<Integer> separateInDigits() {
        List<Integer> digits = new ArrayList<Integer>();
        while(number > 0) {
            digits.add(number % 10);
            number /= 10;
        }
        return digits;
    }

    private boolean detectPrime(int digit) {
        boolean isPrime=true;

        for(int i=2;i<=digit/2;i++)
        {
            if((digit%i)==0)
            {
                isPrime=false;
                break;
            }
        }

        return isPrime;
    }

    private boolean detectPerfect(int digit) {
        if (digit == 0) return false;

        int squareRoot = (int) Math.sqrt(digit);
        return squareRoot*squareRoot ==  digit;
    }


}
